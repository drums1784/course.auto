"""Напишите родительский класс “Фигура” (длина стороны, количество сторон) который считает периметр. 
Этот класс наследуют три других: треугольник, четырехугольник, шестиугольник. Каждый из классов считает площадь фигуры. 
Команда “print(fig1)” должна выводить длину стороны, тип фигуры (3 - треугольник, 4 - квадрат, 6 - гексагон), 
периметр и площадь. По одной фигуре каждого типа."""

class shape:
    
    def __init__(self, side_length, sides_number):
        self.side_length = side_length
        self.sides_number = sides_number

    def perimeter(self):
        return self.side_length * self.sides_number
    
    def __str__(self):
        return f"Длина стороны: {self.side_length}\nКоличество сторон: {self.sides_number}\nПериметр: {self.perimeter()}"


class triangle(shape):

    def area_triangl(self):
        p = self.perimeter() / 2

        return (p * (p - self.side_length) * (p - self.side_length) * (p - self.side_length)) ** 0.5
    
    def __str__(self):
        return f"Тип: треугольник\n{super().__str__()}\nПлощадь: {self.area_triangl()}\n"

class quadrilateral(shape):

    def area_quadrilateral (self):
        return self.side_length * self.side_length
    
    def __str__(self):
        return f"Тип: квадрат\n{super().__str__()}\nПлощадь: {self.area_quadrilateral()}\n"
        
class hexagon(shape):

    def area_hexagon(self):
        return 3 * ((3 ** 0.5) * self.side_length ** 2) / 2
    
    def __str__(self):
        return f"Тип: гексагон\n{super().__str__()}\nПлощадь: {self.area_hexagon()}\n"

