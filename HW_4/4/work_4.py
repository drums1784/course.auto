"""Напишите класс, представляющий собой записную книжку. Каждый элемент записной книжки должен содержать следующие поля: 
ФИО, но­мер телефона, e-mail, день рождения. Записная книжка может сохраняться на диск в виде json-файла, 
а также должна иметь метод загрузки данных из файла."""

from class_4 import *

my_adr = AddressBook()

my_adr.add_entry("Igor Polonsky", "+79123456789", "Polonsky@mail.ru", "01.01.1980")
my_adr.add_entry("Vladimir Askerko", "+375336996788", "askerko@gmail.com", "10.12.2000")

# выводим добавленные данные
my_adr.prin_t() 

my_adr.save_file("HW_4/4/book1.json")

# просто разделитель, между выводами данных
print('-------------------------')

new_my_adr = AddressBook()

new_my_adr.load_file("HW_4/4/address.json")

# выводим загруженные данные и файла
new_my_adr.prin_t()