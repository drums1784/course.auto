"""Напишите класс, представляющий собой записную книжку. Каждый элемент записной книжки должен содержать следующие поля: 
ФИО, но­мер телефона, e-mail, день рождения. Записная книжка может сохраняться на диск в виде json-файла, 
а также должна иметь метод загрузки данных из файла."""

import json

class AddressBook:

    def __init__(self):
        self.entries = []

    def add_entry(self, full_name, phone_number, email, birthday):
        self.full_name = full_name
        self.phone_number = phone_number
        self.email = email
        self.birthday = birthday
        
        entry = {
            'name': full_name,
            'phone_number': phone_number,
            'email': email,
            'birthday': birthday
            }
        self.entries.append(entry)

    def save_file(self, file_name):
        with open(file_name, 'w') as file:
            json.dump(self.entries, file, ensure_ascii=False, indent=4)

    def load_file(self, file_name):
        with open(file_name, 'r') as file:
            self.entries = json.load(file)

    def prin_t(self):
        for i, entry in enumerate(self.entries, start=1):
            print('Запись ',i)
            print("\t", "name:", entry ['name'])
            print("\t", "phone: ", entry ['phone_number'])
            print("\t", "email: ", entry ['email']) 
            print("\t", "birthday: ", entry ['birthday'])
            print()