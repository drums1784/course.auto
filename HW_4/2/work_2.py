""" Напишите родительский класс “Человек” (имя, возраст, пол) и его подклассы “Преподаватель” (предмет, стаж) и 
“Студент” (название специальности, курс). Все переменныеназывать фамилиями. Команда “print(Ivanov)” должна выдать 
всю указанную информацию. Три преподавателя, три студента."""

import class_2

Ivanov = class_2.teacher("Иванов", 50, "мужской", "химия", 11)
Polonsky = class_2.teacher("Polonsky", 38, "мужской", "QA", 2)
Petrova = class_2.teacher("Петрова", 12, "женский", "биология", 100)

Homich = class_2.student("Хомич", 25, "женский", "физика", 23)
Kozel = class_2.student("Козел", 55, "мужской", "экономика", 55)
Baranova = class_2.student("Баранова", 20, "женский", "языки", 11)

print(Ivanov)
print(Polonsky)
print(Petrova)

print(Homich)
print(Kozel)
print(Baranova)