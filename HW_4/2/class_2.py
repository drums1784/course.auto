""" Напишите родительский класс “Человек” (имя, возраст, пол) и его подклассы “Преподаватель” (предмет, стаж) и 
“Студент” (название специальности, курс). Все переменныеназывать фамилиями. Команда “print(Ivanov)” должна выдать 
всю указанную информацию. Три преподавателя, три студента."""

class person:
    
    def __init__(self, name, age, gender):
        self.name = name
        self.age = age
        self.gender = gender


class teacher(person):

    def __init__(self, name, age, gender, subject, experience):
        super().__init__(name, age, gender)
        self.subject = subject
        self.experience = experience

        print ('Данные УЧИТЕЛЯ: Имя - ', self.name, ', возраст: ', self.age, ', пол: ', self.gender, \
               ', предмет: ', self.subject, ', опыт: ', self.experience)


class student(person):

    def __init__(self, name, age, gender, specialty, course):
        super().__init__(name, age, gender)
        self.specialty = specialty
        self.course = course

        print ('Данные СТУДЕНТА: Имя - ', self.name, ', возраст: ', self.age, ', пол: ', self.gender, \
               ', специальность: ', self.specialty, ', курс: ', self.course)
