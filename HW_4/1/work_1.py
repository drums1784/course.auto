"""Напишите класс, реализующий все арифметические операции (a+b, a-b, a*b, a/b, a**b, b**a) над двумя значениями (а и b). 
– В случае, если одно из значений при вызове операции не является числом, выводится сообщение “Not a number!”. 
– Если при делении знаменатель равен 0, вывести сообщение “Cant divide by zero”, заменить 0 на 1 и продолжить вычисление.
"""

import class_1

data = class_1.ArithmeticOper(15, '3')

# Вызов методов класса
print(data.adding())             
print(data.subtraction())          
print(data.multiplication())       
print(data.division())             
print(data.exponentiation_a_b())   
print(data.exponentiation_b_a()) 







