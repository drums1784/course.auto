"""Напишите класс, реализующий все арифметические операции (a+b, a-b, a*b, a/b, a**b, b**a) над двумя значениями (а и b). 
– В случае, если одно из значений при вызове операции не является числом, выводится сообщение “Not a number!”. 
– Если при делении знаменатель равен 0, вывести сообщение “Cant divide by zero”, заменить 0 на 1 и продолжить вычисление.
"""

class ArithmeticOper:
    
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def adding(self):
        try:
            return self.a + self.b
        except TypeError:
            return "Not a number!"
    
    def subtraction(self):
        try:
            return self.a - self.b
        except TypeError:
            return 'Not a number!'
            
    def multiplication(self):
        if not isinstance(self.a, (int, float)) or not isinstance(self.b, (int, float)):
            return"Not a number!"
        
        return self.a * self.b
    
    def division(self):    
        try:
            if self.b == 0:
                print('Cant divide by zero')
                self.b = 1
            return self.a / self.b
        except TypeError:
            return 'Not a number!'
                
    def exponentiation_a_b(self):
        try:
            return self.a ** self.b
        except TypeError:
            return 'Not a number!'
            
    def exponentiation_b_a(self):
        try:
            return self.b ** self.a
        except TypeError:
            return 'Not a number!'
            
    