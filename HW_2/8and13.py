"""13. [В файле 8 задания] Декорируйте функцию из 8 задания таким образом, чтобы возвращаемое 
ей значение возводилось в квадрат."""


def big(fn):
   def wrapper(a, b, c):
       step=fn(a, b, c)
       return int(step) ** 2
   return wrapper

@big

# 8. [Отдельный файл] Напишите функцию, вычисляющую максимальное из трех чисел. Два вызова функции.

def maximum(a, b, c): 
    if (a >= b) and (a >= c): 
        max = a 
    elif (b >= a) and (b >= c): 
        max = b 
    else: 
        max = c 
    return max 

a = input('Введите число 1: ')
b = input('Введите число 2: ')
c = input('Введите число 3: ')

print('Максимальное число в квадрате = ',maximum(a, b, c))