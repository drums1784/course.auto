    # 1. Написать скрипт, который проводит операции (+, -, *, /) с целыми числами.
    a=int(input('Enter 1 number: '));
    b=int(input('Enter 2 number: '));
    c=input('Enter Знак ');
    if c == '+': print ("ANSW: ", a + b)
    elif c == '-': print("ANSW: ",a-b)
    elif c == '*': print("ANSW: ", a*b)
    elif c == '/': print("ANSW: ", round(a/b)) #round округляет результат вычисления (может не надо было делать)
    else: print('Введите правильный символ')


    # 2. Написать скрипт, который проводит операции (+, -, *, /) с  числами.
    a=float(input('enter 1 number: '));
    b=float(input('Enter 2 number: '));
    c=input('Enter Знак ');
    if c == '+': print ("ANSW: ", a + b)
    elif c == '-': print("ANSW: ",a-b)
    elif c == '*': print("ANSW: ", a*b)
    elif c == '/': print("ANSW: ", a/b) #round округляет результат вычисления (может не надо было делать)
    else: print('Введите правильный символ')


    # 3. Написать скрипт, который вычисляет квадратный корень для чисел: 625, 121, 123,4321, 0,015129

    print("Корень квадратный 625 = ", 625 ** 0.5)
    print("Корень квадратный 123 = ", 121 ** 0.5)
    print("Корень квадратный 123,4321 = ", 123,4321 ** 0.5)
    print("Корень квадратный 0.015129 = ", 0,15129e1 ** 0.5)


    # 4. Написать скрипт, который вычисляет целую часть от деления: 150 на 30, 123 на 456 , 4,5 на 4

    print("Целая часть от деления 150/30 = ", 150 // 30)
    print("Целая часть от деления 123/456 = ", 123 //456)
    print("Целая часть от деления 4.5/4 = ", 4.5 // 4)


    # 5. Написать скрипт, который вычисляет остаток от деления: 140 на 30, 456 на 123, 4 на 4,5

    print("Остаток от деления 140/30 = ", 140 % 30)
    print("Остаток от деления 456/123 = ", 456 % 123)
    print("Остаток от деления 4/4.5 = ", 4 % 4.5)

    # 6. Напишите скрипт, вычисляющий двумя способами длину строки.

    stroke='Kavichki56';
    print("Длина строки = ", len(stroke))

    def findLen(stroka):
        counter = 0    
        for i in stroka:
            counter += 1
        return counter
    stroka = "otuароп ап овап вош лрs"
    print(findLen(stroka))


    # 7. Напишите скрипт, который заменяет произвольный символ/букву в строке на $».

    text=str(input('Vvedite text'))
    letter=str(input('Vvedite bukvu '))
    text=text.replace(letter, '$')
    print(text)

    # 8. Напишите скрипт, который позволяет из строки собрать другую по следующим правилам: новая строка должна 
    #состоять из двух первых и двух последних элементов исходной строки. Если длина исходной строки меньше двух,
    #то результатом будет пустая строка.

    str2 = input('Vvedite stroku: ')
    if len(str2) < 2:
        print()
    else:
        result = str2[:2] + str2[-2:]
        print(result)

    # 9. Напишите скрипт, который позволяет инвертировать последователь­ность элементов в строке.

    str=input('Vvedite text: ')
    print(str [::-1])

    # 10.	Напишите скрипт, выводящий все элементы строки с их номерами индексов.

    a='Ivan chai'
    foo = {x : [] for x in a} # создаем словарь с уникальными значениями в котором генерируем список из значений в строке а
    for i,x in enumerate(a): #enumerate возвращает объект в формате пар ключ-значение.
        foo[x].append(i) # методом append добавляем  индекс i для каждого значения в списке х
    print(foo)



    # 11. Поменяйте регистр элементов строки на противоположный.

    message= input('Vvedite message ')
    print(message.swapcase())


    # 12. Выведите символ, который встречается в строке чаще, чем остальные.

    s = input('введите строку для подсчета количества букв: ')
    max_count = 0
    max_char = ''
    for i in range(len(s)):
        if s.count(s[i]) >= max_count:
            max_count = s.count(s[i])
            max_char = s[i]
    print(max_char)


    stroka = input('введите строку для подсчета количества букв: ')
    max_count = 0
    empty_spis = [] 
    for e in set(stroka):
    schet = 0
    for c in stroka:
        if c == e:
            schet += 1
            if schet == max_count:
                max_count = schet
                empty_spis.append(e)
        if schet > max_count:
            max_count = schet
            empty_spis.clear()
            empty_spis.append(e)
    print(empty_spis, " встречаются чаще")

    # 13. Вычислите сумму элементов (чисел) в списке двумя разными спосо­бами.

    spisok = [1, 2, 3, 4, 5]
    print(spisok)
    summa_spiska = sum(spisok)
    print("Summa spiska = ", summa_spiska)


    spis = [1, 2, 3, '4', 5, [2,3]]
    total = 0
    for num in spis:
    try:
        total += num
    except TypeError:
        print(f"Элемент {num} не является числом")
    print(f"Сумма чисел в списке: {total}")



    # 14.Умножьте каждый элемент списка на произвольное число.
    mnozhitel=int(input('Vvedite chislo: '))
    spisok111 = [1, 22, 3, 7, 5]
    new_spisok111 = [i * (mnozhitel) for i in spisok111]
    print(new_spisok111)


    # 15.Напишите скрипт для слияния (конкатенации) двух списков тремя различ­ными способами.
    list1 = [100, 1, 12, 1, 14] 
    list2 = [20, 30, 42] 
    result1 = list1 + list2 
    print ("Spisok obedenenniy: " + str(result1))


    list122 = [10, 1, 12, 3, 154] 
    list222 = [20, 30, 42] 
    resul_111 = [*list122, *list222]
    print ("List122 + List222 = : " + str(resul_111))


    list_1 = [10, 'r', 12, 13, 14] 
    list_2 = [20, 30, '42eee'] 
    for x in list_2 : list_1.append(x) 
    print ("New list = : " + str(list_1))

    # 16.	Напишите скрипт, меняющий позициями элементы списка с индек­сами n и n+1

    massiv = [1, 2, 3, 4]
    n = int(input("Введите индекс списка: "))
    m = int(len(massiv))
    if n > m-1:
        print ('Нет такого индекса')
    if n == m-1:
        t=massiv[0]
        massiv[0]=massiv[n]
        massiv[n]=t
    else: 
        x = massiv[n]
        massiv[n]=massiv[n+1]
        massiv[n+1]=x
    print (massiv)



    #17. Напишите скрипт, переводящий список из различного количества числовых целочисленных элементов в одно число. 
    #Пример списка: [1, 23, 456], результат: 123456.

    sp=[1,23,456]
    n_s=[str(i) for i in sp]
    print("".join(n_s))


    # 18. Добавьте еще несколько пар «ключ: значение» в следующий словарь: {0: 10, 1: 20}.

    slovar = {0: 10, 1: 20, 2:'ttt'}
    slovar.update(dict(rost = '179', ven = 1))
    print("Slovar dobavlenni: ", slovar)

    # 19. Напишите скрипт, который из трех словарей создаст один новый.

    sl1={1:10, 2:20}
    sl2={3:30, 4:40}
    sl3={5:50,6:60}
    sl4 = {}
    for d in (sl1, sl2, sl3): sl4.update(d)
    print("New slovar: ", sl4)


    # 20. Напишите скрипт для удаления элемента словаря.

    sl = {'Igor': 37, 'Slava': 48, 'Sveta': 35}
    del sl['Igor']
    print("Slovar update after delete: ", sl)


    # 21. Напишите скрипт, проверяющий, существует ли заданный ключ в словаре.
    
    d = {"key1": 10, "key2": 23}
    s=input()
    if s in d:
        print("Yes")

    else:
        print("No")

        
    # 22. Объявите и инициализируйте кортеж различными способами, после чего распакуйте его.

    tuple_1 = (22, 12, 32)
    x, y, z = tuple_1
    print("Kortezh: ", x, y, z)

    numbers = (11, 21, 31)
    numbers2 = 1, 22, 32
    print(numbers)
    print(numbers2)

    Z = tuple(["Pyt", "Mat", "Mac"])
    print(Z)


    # 23. Конвертируйте список в кортеж, затем добавьте в кортеж новые элементы.

    list_x = [5, 10, 7, 4, 15, 3]
    tuplex = tuple(list_x)
    new_tuple = tuplex + (113, 'aaa')
    print(new_tuple)


    # 24. Конвертируйте кортеж в словарь.

    t = [('a', 1), ('b', 2), ('c', 3), ('a', 4)]
    d = dict()
    for x,y in t:
        d[x] = d.get(x, 0) + y;
    print(d)


    # 25. Напишите скрипт, подсчитывающий количество элементов типа кортеж в списке.

    spisok_2 = [11, 'a', (2, 42), 3, (6, 'm'), (1, 77), 55, 'ddd']
    kort = 0
    for i in spisok_2:
        if type(i) == tuple :
            kort += 1
    print(kort)


    # 26. Объявите и инициализируйте множество тремя различными способами.
    mixed_set = {2.0, "Nicholas", (1, 2, 3)}  
    print(mixed_set)

    MySpis = [1, 2, 3, 4, 4, 5, 5, 6, 7, 8, 9, 10]
    NewSpis = {element for element in MySpis}
    print(NewSpis)

    num_set = set([1, 2, 3, 4, 5, 6])  
    print(num_set)


    # 27. Удалите элемент из множества.

    a = { "a", "b" , "c" }
    a.remove("b")
    print(a)


    # 28. Удалите повторяющиеся элементы из списка.
    list1 = [1, 5, 3, 6, 3, 5, 6, 1, 1, 9]
    list1 = list(set(list1))
    print ("List without dublicatov: ", list1)


    # 29. Напишите скрипт, определяющий длину множества двумя различными способами.

    names_a = {"Nicholas", "Michelle", "John", "Mercy"}
    print(len(names_a)) 


    mnozh = {1.1, 1.2, 1.3, 1.4, 1.5}
    total=0
        for i in mnozh:
        total += 1
    print(total)


    # 30. Напишите скрипт для проверки, входит ли элемент в множество.

    my_set = set(input('VVedite mnozhestvo: '))
    ff = input('Vvedite simvol for checked: ')
    print(ff in my_set)
