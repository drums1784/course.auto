# 13. Вычислите сумму элементов (чисел) в списке двумя разными спосо­бами.

#var 1

def sumlistchisla(spisok):
    summa_spiska = sum(spisok)

    print("Summa spiska = ", summa_spiska)

#var 2

def sumlisttwo(spis):
    total = 0

    for num in spis:
        try:
            total += num

        except TypeError:
            print(f"Элемент {num} не является числом")

    print(f"Сумма чисел в списке: {total}")


# 14.Умножьте каждый элемент списка на произвольное число.

def listmnozh(mnozhitel, spisok111):
    new_spisok111 = [i * (mnozhitel) for i in spisok111]

    print(new_spisok111)


# 15.Напишите скрипт для слияния (конкатенации) двух списков тремя различ­ными способами.

#var 1

def plaast(list1, list2):
    result1 = list1 + list2 

    print ("Spisok obedenenniy: " + str(result1))

#var 2

def plaasttwo(list3, list4):
    resul_111 = [*list3, *list4]

    print ("List122 + List222 = : " + str(resul_111))

#var 3

def plaastthree(list_1, list_2):

    for x in list_2 : list_1.append(x)

    print ("New list = : " + str(list_1))


# 16.	Напишите скрипт, меняющий позициями элементы списка с индек­сами n и n+1

def men_poz_list(massiv, n):
    m = int(len(massiv))

    if n > m-1:
        print ('Нет такого индекса')

    if n == m-1:
        t = massiv[0]
        massiv[0] = massiv[n]
        massiv[n] = t
    else: 
        x = massiv[n]
        massiv[n] = massiv[n+1]
        massiv[n+1] = x

    print (massiv)


#17. Напишите скрипт, переводящий список из различного количества числовых целочисленных элементов в одно число. 
    #Пример списка: [1, 23, 456], результат: 123456.

def uslist(sp):
    n_s = [str(i) for i in sp]

    print("".join(n_s))


# 28. Удалите повторяющиеся элементы из списка.

def del_povtor(list1):
    list1 = list(set(list1))

    print ("List without dublicatov: ", list1)



#------------------------------------------------

#HW 2


"""4.Посчитайте количество вхождений элемента со 
значением «3» тремя способами в сле­дующем списке: [3 0 1 3 0 4 3 3 4 5 6 6 1 3], 
используя цикл for, while и метод count."""

#var 1

def kolvo_elem_sp(numb):
    coun = 0
    a = 0

    while coun<len(numb):
        if numb[coun] == 3:
           a = a + 1
        coun += 1

    print('Троек в списке: ',a)


#var 2

def kolvo_elem_sp2(numb2):
    coun = numb2.count(3)

    print('Троек в списке: ',coun)


#var 3

def kolvo_elem_sp3(numb3):
    coun = 0

    for i in numb3:
        if i == 3:
            coun = coun + 1

    print('Троек в списке: ',coun)


#5. Напишите программу, выводящую среднее из десяти значений.

def avg_list(numb):
    sum_sp = 0
    coun = 0

    for i in numb:
        sum_sp = sum_sp+i
        coun = coun+1
        avr = sum_sp/coun
    
    print(sum_sp)
    print(coun)
    print('Среднее число списка из',coun, 'значений равно: ',avr)


#11. Напишите функцию, удаляющую повторяющиеся элементы в списке. Три вызова функции.

def repit(a):
    temp = [] 

    for x in a: 
        if x not in temp: 
            temp.append(x) 
    return temp

