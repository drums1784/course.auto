# 18. Добавьте еще несколько пар «ключ: значение» в следующий словарь: {0: 10, 1: 20}.

def addpar(a):
    a.update(dict(rost = '179', ven = 1))

    print("Slovar dobavlenni: ", a)


# 19. Напишите скрипт, который из трех словарей создаст один новый.

def one_new_slov(sl1,sl2,sl3):
    sl4 = {}
    for d in (sl1, sl2, sl3): sl4.update(d)

    print("New slovar: ", sl4)


# 20. Напишите скрипт для удаления элемента словаря.

def del_elem2(sl):
    del sl['Igor']

    print("Slovar update after delete: ", sl)


# 21. Напишите скрипт, проверяющий, существует ли заданный ключ в словаре.
    
def findkey(a,b):
    if b in a:
        print("Yes")
    else:
        print("No")


# 22. Объявите и инициализируйте кортеж различными способами, после чего распакуйте его.

#var 1

def initkort(tuple):  
    x, y, z = tuple

    print("Kortezh: ", x, y, z)

#var 2

def initkort2(numbers):
    print(numbers)

#var 3

def init_kort(z):
    print(z)


# 23. Конвертируйте список в кортеж, затем добавьте в кортеж новые элементы.

def konvert_list(list_x, b):
    tuplex = tuple(list_x)
    new_tuple = tuplex + b

    print(new_tuple)
        

# 24. Конвертируйте кортеж в словарь.

def konv_in_slov(t):
    d = dict()
    for x,y in t:
        d[x] = d.get(x, 0) + y

    print(d)


# 25. Напишите скрипт, подсчитывающий количество элементов типа кортеж в списке.

def kolvo_elem(spisok_2):
    kort = 0

    for i in spisok_2:
        if type(i) == tuple :
            kort += 1

    print(kort)


# 26. Объявите и инициализируйте множество тремя различными способами.

#var 1

def init_mnozh1(a):
    print(a)

#var 2
   
def init_mnozh2(a):
    NewSpis = {element for element in a}

    print(NewSpis)

#var 3

def init_mnozh3(a):
    print(a)


# 27. Удалите элемент из множества.

def del_elem(a,b):
    a.remove(b)

    print(a)


# 29. Напишите скрипт, определяющий длину множества двумя различными способами.

#var 1

def len_mnozh(a):
    print(len(a)) 

#var 2

def len_mnozh2(aa): 
    total = 0

    for i in aa:
        total += 1

    print(total)


# 30. Напишите скрипт для проверки, входит ли элемент в множество.

def find_elem_mnozh(a,b):
    print(b in a)